﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Config
{
    /// <summary>
    /// 基础配置
    /// </summary>
    public class AspNetCoreConfig
    {
        /// <summary>
        /// Forms的域名
        /// </summary>
        public string Domain { get; set; }


    }
}
