﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AspNetCore.Web.Data
{
    /// <summary>
    /// Repository 操作类
    /// </summary>
    public interface IRepository<TEntity> where TEntity :class 
    {
        /// <summary>
        /// DbContext
        /// </summary>
        DbContext DbContext { get; }

        /// <summary>
        /// 实体
        /// </summary>
        DbSet<TEntity> Entities { get; }

        /// <summary>
        /// 获取单个实例数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity GetById(object id);

        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// 批量新增
        /// </summary>
        /// <param name="entities"></param>
        void Insert(IEnumerable<TEntity> entities);

        /// <summary>
        /// 更新数据
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// 批量更新
        /// </summary>
        /// <param name="entities"></param>
        void Update(IEnumerable<TEntity> entities);

        /// <summary>
        /// 删除数据
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="entities"></param>
        void Delete(IEnumerable<TEntity> entities);

        /// <summary>
        /// 数据表
        /// </summary>
        IQueryable<TEntity> Table { get; }

        /// <summary>
        /// 无跟踪表
        /// </summary>
        IQueryable<TEntity> TableNoTracking { get; }
    }
}
