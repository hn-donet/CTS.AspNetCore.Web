﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    /// <summary>
    /// enum扩展
    /// </summary>
    public static class EnumExtensions
    {
        /// <summary>
        /// 将enum转成 SelectList
        /// </summary>
        /// <param name="enumValue"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToSelectList(this Enum enumValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            foreach (var item in Enum.GetValues(enumValue.GetType()))
                items.Add(new SelectListItem() { Text = item.ToString(), Value = ((int)item).ToString() });

            return items;
        }
    }
}
