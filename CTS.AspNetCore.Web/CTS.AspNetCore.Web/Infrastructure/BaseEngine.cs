﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace AspNetCore.Web.Infrastructure
{
    /// <summary>
    /// 基础引起
    /// </summary>
    public abstract class BaseEngine : IEngine
    {
        private IConfiguration _configuration;
        private IServiceProvider _serviceProvider { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public BaseEngine(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        /// <summary>
        /// 配置对象
        /// </summary>
        public virtual IConfiguration Configuration => _configuration;

        /// <summary>
        /// 
        /// </summary>
        public virtual IServiceProvider ServiceProvider => _serviceProvider;

        /// <summary>
        /// 配置ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            _serviceProvider = services.BuildServiceProvider();

            return _serviceProvider;
        }

        /// <summary>
        /// 从写初始化方法
        /// </summary>
        /// <param name="services"></param>
        public abstract void Initialize(IServiceCollection services);
         
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T Resolve<T>() where T : class
        {
            return (T)GetServiceProvider().GetRequiredService(typeof(T));
        }

        /// <summary>
        /// 获取ServiceProvider
        /// </summary>
        /// <returns></returns>
        protected IServiceProvider GetServiceProvider()
        {
            var accessor = ServiceProvider.GetService<IHttpContextAccessor>();
            var context = accessor.HttpContext;
            return context != null ? context.RequestServices : ServiceProvider;
        }
    }
}
