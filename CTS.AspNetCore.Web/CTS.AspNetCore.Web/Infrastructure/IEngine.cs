﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEngine
    {
        /// <summary>
        /// 配置对象
        /// </summary>
        IConfiguration Configuration { get; }

        /// <summary>
        /// 配置文件，从写此方法
        /// </summary>
        /// <param name="services"></param>
        void Initialize(IServiceCollection services);

        /// <summary>
        /// 配置ConfigureServices
        /// </summary>
        /// <param name="services"></param>
        /// <returns></returns>
        IServiceProvider ConfigureServices(IServiceCollection services);

        /// <summary>
        /// 构建实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T Resolve<T>() where T : class;








    }
}
