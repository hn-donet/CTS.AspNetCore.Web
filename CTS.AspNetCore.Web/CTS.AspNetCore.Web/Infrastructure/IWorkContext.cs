﻿using Microsoft.AspNetCore.Mvc.Filters;
using AspNetCore.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Infrastructure
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWorkContext
    {
        /// <summary>
        /// 登陆人的token
        /// </summary>
        string Token { get; }

        /// <summary>
        /// 当前登录用户
        /// </summary>
        Administrator CurrentUser { get; }

        /// <summary>
        /// 当前登录用户的菜单
        /// </summary>
        List<CategoryInfo> Categories { get; }

        /// <summary>
        /// 当前程序对象
        /// </summary>
        Store Stroe { get; }

        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="routeName"></param>
        /// <returns></returns>
        bool OwnPermission(string routeName);

        /// <summary>
        /// 验证权限
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        bool OwnPermission(ActionExecutingContext context);
    }
}
