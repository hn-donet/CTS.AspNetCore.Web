﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace AspNetCore.Web.Librs
{
    /// <summary>
    /// 安全辅助类
    /// </summary>
    public class EncryptorHelper
    {
        /// <summary>
        /// MD5加密
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string GetMD5(string sourceString)
        {
            MD5 md5 = MD5.Create();
            byte[] source = md5.ComputeHash(Encoding.UTF8.GetBytes(sourceString));
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < source.Length; i++)
            {
                sBuilder.Append(source[i].ToString("x2"));
            }
            return sBuilder.ToString();
        }

        /// <summary>
        /// 生成Salt盐
        /// </summary>
        /// <param name="size">随机数长度，默认32字节</param>
        /// <returns></returns>
        public static string CreateSaltKey(int size = 32)
        {
            var rng = new RNGCryptoServiceProvider();
            var buff = new byte[size];
            rng.GetBytes(buff);
            return Convert.ToBase64String(buff);
        }

        /// <summary>
        /// UTF-8 编码方式的 HmacSha1 加密
        /// </summary>
        /// <param name="key">密钥</param>
        /// <param name="input">加密字符串</param>
        /// <returns>返回16进制大写的加字符串</returns>
        public static string HmacSha1(string key, string input)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            HMACSHA1 hmac = new HMACSHA1(keyBytes);
            byte[] hashBytes = hmac.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hashBytes.Length; i++)
                sb.AppendFormat("{0:X2}", hashBytes[i]);
            return sb.ToString();
        }

        /// <summary>
        /// Algorithm 加密
        /// </summary>
        /// <param name="input"></param>
        /// <param name="base64Key">Base64位</param>
        /// <param name="hashAlgorithmType">加密方式</param>
        /// <returns></returns>
        public static string Algorithm(string input, string base64Key, string hashAlgorithmType = "SHA1")
        {
            byte[] bytes = Encoding.Unicode.GetBytes(input);
            byte[] array = Convert.FromBase64String(base64Key);
            byte[] array2 = new byte[array.Length + bytes.Length];
            byte[] array3 = null;
            Buffer.BlockCopy(array, 0, array2, 0, array.Length);
            Buffer.BlockCopy(bytes, 0, array2, array.Length, bytes.Length);
            HashAlgorithm hashAlgorithm = HashAlgorithm.Create(hashAlgorithmType);
            array3 = hashAlgorithm.ComputeHash(array2);
            return Convert.ToBase64String(array3);
        }



        #region Base64

        /// <summary>
        /// Base64加密，采用utf8编码方式加密
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string EncodeBase64(string sourceString)
        {
            byte[] bt = Encoding.UTF8.GetBytes(sourceString);
            return Convert.ToBase64String(bt);
        }

        /// <summary>
        /// Base64解密，采用utf8编码方式解密
        /// </summary>
        /// <param name="sourceString"></param>
        /// <returns></returns>
        public static string DecodeBase64(string sourceString)
        {
            byte[] bt = Convert.FromBase64String(sourceString);
            return Encoding.UTF8.GetString(bt);
        }

        #endregion

        /// <summary>
        /// 使用加密服务提供程序实现加密生成随机数
        /// validationKey 的有效值为 20 到 64。
        /// decryptionKey 的有效值为 8 或 24。
        /// </summary>
        /// <param name="numBytes"></param>
        /// <returns>16进制格式字符串</returns>
        public static string CreateMachineKey(int numBytes)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[numBytes];
            rng.GetBytes(buff);
            System.Text.StringBuilder hexString = new System.Text.StringBuilder(64);
            for (int i = 0; i < buff.Length; i++)
            {
                hexString.Append(String.Format("{0:X2}", buff[i]));
            }
            return hexString.ToString();
        }

        #region DES 加密解密

        /// <summary>
        /// Des加密方法
        /// </summary>
        /// <param name="val">待加密数据</param>
        /// <param name="key">8位字符</param>
        /// <param name="iv">8位字符</param>
        /// <returns></returns>
        public static string DESEncrypt(string val, string key, string iv)
        {
            try
            {
                byte[] byKey = System.Text.ASCIIEncoding.ASCII.GetBytes(key);
                byte[] byIV = System.Text.ASCIIEncoding.ASCII.GetBytes(iv);

                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                int i = cryptoProvider.KeySize;
                MemoryStream ms = new MemoryStream();
                CryptoStream cst = new CryptoStream(ms, cryptoProvider.CreateEncryptor(byKey, byIV), CryptoStreamMode.Write);

                StreamWriter sw = new StreamWriter(cst);
                sw.Write(val);
                sw.Flush();
                cst.FlushFinalBlock();
                sw.Flush();
                return Convert.ToBase64String(ms.GetBuffer(), 0, (int)ms.Length);
            }
            catch// (Exception ex)
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Des解密方法
        /// </summary>
        /// <param name="val">待解密数据</param>
        /// <param name="key">8位字符</param>
        /// <param name="iv">8位字符</param>
        /// <returns></returns>
        public static string DESDecrypt(string val, string key, string iv)
        {
            try
            {
                byte[] byKey = ASCIIEncoding.ASCII.GetBytes(key);
                byte[] byIV = ASCIIEncoding.ASCII.GetBytes(iv);

                byte[] byEnc;
                try
                {
                    byEnc = Convert.FromBase64String(val);
                }
                catch
                {
                    return null;
                }
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                MemoryStream ms = new MemoryStream(byEnc);
                CryptoStream cst = new CryptoStream(ms, cryptoProvider.CreateDecryptor(byKey, byIV), CryptoStreamMode.Read);
                StreamReader sr = new StreamReader(cst);
                return sr.ReadToEnd();
            }
            catch// (System.Exception ex)
            {
                return String.Empty;
            }
        }

        #endregion

        #region RSA 加密解密

        /// <summary>
        /// 生成一对秘钥。arr[0]私钥，arr[1]公钥
        /// </summary>
        /// <returns></returns>
        public static string[] GenerateRSAKeys()
        {
            string[] sKeys = new String[2];
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            sKeys[0] = rsa.ToXmlString(true);
            sKeys[1] = rsa.ToXmlString(false);
            return sKeys;
        }

        ///<summary>
        /// RSA加密
        /// </summary>
        /// <param name="publickey">公钥</param>
        /// <param name="content">加密内容</param>
        /// <returns></returns>
        public static string RSAEncrypt(string publickey, string content)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            byte[] cipherbytes;
            rsa.FromXmlString(publickey);
            cipherbytes = rsa.Encrypt(Encoding.UTF8.GetBytes(content), false);
            return Convert.ToBase64String(cipherbytes);
        }

        /// <summary>
        /// RSA解密
        /// </summary>
        /// <param name="privatekey">私钥</param>
        /// <param name="content">解密内容</param>
        /// <returns></returns>
        public static string RSADecrypt(string privatekey, string content)
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            byte[] cipherbytes;
            rsa.FromXmlString(privatekey);
            cipherbytes = rsa.Decrypt(Convert.FromBase64String(content), false);

            return Encoding.UTF8.GetString(cipherbytes);
        }

        #endregion

        #region 3DES 加密解密

        /// <summary>
        /// UTF8 编码 3DES 加密
        /// </summary>
        /// <param name="input">待加密字符串</param>
        /// <param name="key">24字节的秘钥byte数组</param>
        /// <param name="mode"></param>
        /// <param name="iv">CBC 必须，8 位加密向量</param>
        /// <returns>Base64字符串</returns>
        public static string Encrypt3DES(string input, byte[] key, CipherMode mode = CipherMode.ECB, string iv = null)
        {
            var des = new TripleDESCryptoServiceProvider
            {
                Key = key,
                Mode = mode
            };
            if (mode == CipherMode.CBC)
                des.IV = Encoding.UTF8.GetBytes(iv);
            var desEncrypt = des.CreateEncryptor();
            byte[] buffer = Encoding.UTF8.GetBytes(input);
            return Convert.ToBase64String(desEncrypt.TransformFinalBlock(buffer, 0, buffer.Length));
        }

        /// <summary>
        /// 3DES 解密
        /// </summary>
        /// <param name="input">3DES 加密得到的Base64字符串</param>
        /// <param name="key">24字节的密钥字节数组</param>
        /// <param name="mode"></param>
        /// <param name="iv">CBC 必须填写iv。8位</param>
        /// <returns></returns>
        public static string Decrypt3DES(string input, byte[] key, CipherMode mode = CipherMode.ECB, string iv = null)
        {
            var des = new TripleDESCryptoServiceProvider
            {
                Key = key,
                Mode = mode,
                Padding = PaddingMode.PKCS7
            };
            if (mode == CipherMode.CBC)
                des.IV = Encoding.UTF8.GetBytes(iv);
            var desDecrypt = des.CreateDecryptor();
            var result = "";
            byte[] buffer = Convert.FromBase64String(input);
            result = Encoding.UTF8.GetString(desDecrypt.TransformFinalBlock(buffer, 0, buffer.Length));
            return result;

        }

        /// <summary>
        /// utf-8 编码的 3des 加密,并转为16进制的字符串。
        /// 48位 16进制 的秘钥加密
        /// </summary>
        /// <param name="input">待加密的字符串</param>
        /// <param name="hexKey">48位 16进制 的秘钥加密</param>
        /// <returns></returns>
        public static string ECBEncrypt3DESHex(string input, string hexKey)
        {
            var des = new TripleDESCryptoServiceProvider
            {
                Key = HexToBytes(hexKey),
                Mode = CipherMode.ECB
            };
            var desEncrypt = des.CreateEncryptor();
            byte[] buffer = Encoding.UTF8.GetBytes(input);
            byte[] result = desEncrypt.TransformFinalBlock(buffer, 0, buffer.Length);
            return ByteToHex(result);
        }

        /// <summary>
        /// utf-8 编码的 解密3des加密成16进制的字符串
        /// 48位 16进制 的秘钥加密
        /// </summary>
        /// <param name="hexInput">16进制的加密字符串</param>
        /// <param name="hexKey">48位 16进制 的秘钥加密</param>
        /// <returns></returns>
        public static string ECBDecrypt3DESByHex(string hexInput, string hexKey)
        {
            var des = new TripleDESCryptoServiceProvider
            {
                Key = HexToBytes(hexKey),
                Mode = CipherMode.ECB
            };
            var desDecrypt = des.CreateDecryptor();
            byte[] buffer = HexToBytes(hexInput);
            byte[] result = desDecrypt.TransformFinalBlock(buffer, 0, buffer.Length);
            return Encoding.UTF8.GetString(result);
        }


        #endregion

        #region 进制转换

        /// <summary>
        /// 字节数组转16进制字符串
        /// </summary>
        /// <param name="bytes">字节数组</param>
        /// <returns></returns>
        public static string ByteToHex(byte[] bytes)
        {
            StringBuilder sb = new StringBuilder();
            if (bytes != null)
                for (int i = 0; i < bytes.Length; i++)
                    sb.Append(bytes[i].ToString("X2"));
            return sb.ToString();
        }

        /// <summary>
        /// 字符串转16进制字节数组
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>
        public static byte[] HexToBytes(string hexString)
        {
            hexString = hexString.Replace(" ", "");
            if ((hexString.Length % 2) != 0)
                hexString += " ";
            byte[] returnBytes = new byte[hexString.Length / 2];
            for (int i = 0; i < returnBytes.Length; i++)
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            return returnBytes;
        }

        /// <summary>
        /// 使用加密服务提供程序实现加密生成随机数
        /// </summary>
        /// <param name="numBytes"></param>
        /// <returns></returns>
        public static string CreateRNGKey(int numBytes)
        {
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] buff = new byte[numBytes];
            rng.GetBytes(buff);
            return ByteToHex(buff);
        }



        #endregion

        #region AES 加密解密

        /// <summary>
        /// AES 加密
        /// </summary>
        /// <param name="val"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string Encrypt(string val, string key, string iv)
        {
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Key = Convert.FromBase64String(key);
                rijndaelManaged.IV = Convert.FromBase64String(iv);
                ICryptoTransform cryptoTransform = rijndaelManaged.CreateEncryptor();
                byte[] bytes = Encoding.UTF8.GetBytes(val);
                byte[] inArray = cryptoTransform.TransformFinalBlock(bytes, 0, bytes.Length);
                cryptoTransform.Dispose();
                return Convert.ToBase64String(inArray);
            }
        }

        /// <summary>
        /// AES 解密
        /// </summary>
        /// <param name="text"></param>
        /// <param name="key"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public static string Decrypt(string text, string key, string iv)
        {
            using (RijndaelManaged rijndaelManaged = new RijndaelManaged())
            {
                rijndaelManaged.Key = Convert.FromBase64String(key);
                rijndaelManaged.IV = Convert.FromBase64String(iv);
                ICryptoTransform cryptoTransform = rijndaelManaged.CreateDecryptor();
                byte[] array = Convert.FromBase64String(text);
                byte[] bytes = cryptoTransform.TransformFinalBlock(array, 0, array.Length);
                cryptoTransform.Dispose();
                return Encoding.UTF8.GetString(bytes);
            }
        }

        #endregion
    }
}
