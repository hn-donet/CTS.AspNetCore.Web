﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Librs
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlUitls
    {
        /// <summary>
        /// url添加参数
        /// </summary>
        /// <param name="url"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static string AddParam(string url, params string[] param)
        {
            if (String.IsNullOrEmpty(url))
                throw new ArgumentNullException(nameof(url) + "不能为空");
            url = url.TrimEnd('&');
            if (param != null && param.Length > 0)
            {
                if (url.IndexOf('?') == -1)
                    url += "?";
                foreach (var p in param)
                    url += "&" + p;
            }
            return url;

        }
    }
}
