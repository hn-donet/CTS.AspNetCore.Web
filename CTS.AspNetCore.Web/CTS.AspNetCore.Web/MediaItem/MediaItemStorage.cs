﻿using AspNetCore.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AspNetCore.Web.MediaItem
{
    /// <summary>
    /// 
    /// </summary>
    public class MediaItemStorage : IMediaItemStorage
    {
        private IWebHelper _webHelper;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="webHelper"></param>
        public MediaItemStorage(IWebHelper webHelper)
        {
            _webHelper = webHelper;
        }

        /// <summary>
        /// 文件存储,返回路径的相对路径
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="virtualPath">虚拟相对目录 xxxx/xxx</param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public string Storage(MemoryStream stream, string virtualPath, string fileName)
        {
            string psth = virtualPath.TrimStart('~');
            var physicalPath = _webHelper.MapPath(virtualPath);

            if (string.IsNullOrEmpty(physicalPath))
                throw new NoNullAllowedException("虚拟路径转换错误");
            if (!Directory.Exists(physicalPath))
                Directory.CreateDirectory(physicalPath);

            var fullPath = Path.Combine(physicalPath, fileName);
            using (var outStream = File.OpenWrite(fullPath))
            {
                stream.WriteTo(outStream);
                outStream.Flush();
                outStream.Close();
            }
            return Path.Combine(psth, fileName);
        }
    }
}
