﻿using AspNetCore.Web.Librs;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Menu
{
    /// <summary>
    /// 
    /// </summary>
    public class SiteMapNode
    {
        /// <summary>
        /// 
        /// </summary>
        public SiteMapNode()
        {

        }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string SysResource { get; set; }

        /// <summary>
        /// 统一资源定位标识符 MD5 解析
        /// </summary>
        public string ResourceID
        {
            get
            {
                if (!String.IsNullOrEmpty(SysResource))
                    return EncryptorHelper.GetMD5(SysResource);
                return "";
            }
        }
         
        /// <summary>
        /// 上级统一资源定位标识md5 解析
        /// </summary>
        public string FatherID { get; set; } 

        /// <summary>
        /// 
        /// </summary>
        public bool IsMenu { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string RouteName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Controller { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string IconClass { get; set; }
        
        /// <summary>
        /// 自动读取循序设置
        /// </summary>
        public int Sort { get; set; }
    }
}
