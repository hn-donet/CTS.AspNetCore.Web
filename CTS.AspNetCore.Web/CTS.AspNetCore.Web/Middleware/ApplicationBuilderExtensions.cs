﻿using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationBuilderExtensions
    {
        /// <summary>
        /// 启用浏览器标识，为浏览器添加唯一个固定的cookie，标识浏览器，类似sessionid
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseBrowserDiscern(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<BrowserDiscernMiddleware>();
        }


        /// <summary>
        /// 启用浏览器标识，为浏览器添加唯一个固定的cookie，标识浏览器，类似sessionid
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseBrowserDiscern(this IApplicationBuilder builder,Action<BrowserOptions> action)
        {
            BrowserOptions options = new BrowserOptions();
            action(options);
            return builder.UseMiddleware<BrowserDiscernMiddleware>(options);
        }
        
    }
}
