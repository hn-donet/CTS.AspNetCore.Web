﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// 浏览器中间件
    /// </summary>
    public class BrowserDiscernMiddleware
    {
        private static readonly RandomNumberGenerator CryptoRandom = RandomNumberGenerator.Create();
        private const int BrowserIDLength = 36; 

        private readonly RequestDelegate _next;
        private BrowserOptions _options;
        private IBrowserProtection _browserProtection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        /// <param name="browserProtection"></param>
        /// <param name="options"></param>
        public BrowserDiscernMiddleware(RequestDelegate next, IBrowserProtection browserProtection, BrowserOptions options)
        {
            _next = next;
            _options = options;
            _browserProtection = browserProtection;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            string cookieValue = context.Request.Cookies[_options.Cookie.Name];

            string browserId = _browserProtection.Unprotect(cookieValue);

            if (String.IsNullOrWhiteSpace(browserId) || browserId.Length != BrowserIDLength)
            {
                var guidBytes = new byte[16];
                CryptoRandom.GetBytes(guidBytes);
                cookieValue = _browserProtection.Protect(new Guid(guidBytes).ToString());
                var establisher = new BrowserEstablisher(context, cookieValue, _options);
            }
            await _next.Invoke(context);
        }

        /// <summary>
        /// 
        /// </summary>
        private class StringSerializer : IDataSerializer<string>
        {
            public string Deserialize(byte[] data)
            {
                return Encoding.UTF8.GetString(data);
            }

            public byte[] Serialize(string model)
            {
                return Encoding.UTF8.GetBytes(model);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class BrowserEstablisher
        {
            private readonly HttpContext _context;
            private readonly string _cookieValue;
            private readonly BrowserOptions _options;

            public BrowserEstablisher(HttpContext context, string cookieValue, BrowserOptions options)
            {
                _options = options;
                _context = context;
                _cookieValue = cookieValue;
                context.Response.OnStarting(OnStartingCallback, state: this);
            }

            private static Task OnStartingCallback(object state)
            {
                var establisher = (BrowserEstablisher)state;
                establisher.SetCookie();
                return Task.FromResult(0);
            }


            private void SetCookie()
            {
                var cookieOptions = _options.Cookie.Build(_context);

                _context.Response.Cookies.Append(_options.Cookie.Name, _cookieValue, cookieOptions);

                _context.Response.Headers["Cache-Control"] = "no-cache";
                _context.Response.Headers["Pragma"] = "no-cache";
                _context.Response.Headers["Expires"] = "-1";
            }
        }

    }

    /// <summary>
    /// broswer加密解密处理
    /// </summary>
    public interface IBrowserProtection
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        string Protect(string data);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="protectedText"></param>
        /// <returns></returns>
        string Unprotect(string protectedText);
    }

    /// <summary>
    /// broswer加密解密处理
    /// </summary>
    public class BrowserProtection : IBrowserProtection
    {
        private IDataProtectionProvider _dataProtectionProvider;
        private SecureDataFormat<string> _dataFormat;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataProtectionProvider"></param>
        public BrowserProtection(IDataProtectionProvider dataProtectionProvider)
        {
            _dataProtectionProvider = dataProtectionProvider;
            var dataProtector = _dataProtectionProvider.CreateProtector(typeof(BrowserProtection).FullName);
            _dataFormat = new SecureDataFormat<string>(new StringSerializer(), dataProtector);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string Protect(string data)
        {
            return _dataFormat.Protect(data);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="protectedText"></param>
        /// <returns></returns>
        public string Unprotect(string protectedText)
        {
            try
            {
                if (String.IsNullOrEmpty(protectedText))
                    return string.Empty;
                return _dataFormat.Unprotect(protectedText);
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private class StringSerializer : IDataSerializer<string>
        {
            public string Deserialize(byte[] data)
            {
                return Encoding.UTF8.GetString(data);
            }

            public byte[] Serialize(string model)
            {
                return Encoding.UTF8.GetBytes(model);
            }
        }
    }

    /// <summary>
    /// 浏览器标识上下文
    /// </summary>
    public interface IBrowserContext
    {
        /// <summary>
        /// 获取browserid，默认cookie名称
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        string GetBrowserId(HttpContext context);

        /// <summary>
        /// 获取browserid，指定cookie名称
        /// </summary>
        string GetBrowserId(HttpContext context,string cookieName);
    }

    /// <summary>
    /// 浏览器标识上下文
    /// </summary>
    public class BrowserContext: IBrowserContext
    {
        private IBrowserProtection _browserProtection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="browserProtection"></param>
        public BrowserContext(IBrowserProtection browserProtection)
        {
            _browserProtection = browserProtection;
        }

        /// <summary>
        /// 获取browserid，默认cookie名称
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetBrowserId(HttpContext context)
        { 
            return GetBrowserId(context, BrowserDefaults.CookieName);
        }

        /// <summary>
        /// 获取browserid，指定cookie名称
        /// </summary>
        public string GetBrowserId(HttpContext context, string cookieName)
        {
            string cookieValue = context.Request.Cookies[cookieName];
            return _browserProtection.Unprotect(cookieValue);
        }




    }




}
