﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Microsoft.AspNetCore.Builder
{
    /// <summary>
    /// 
    /// </summary>
    public class BrowserOptions
    {
        private CookieBuilder _cookieBuilder = new BrowserCookieBuilder();

        /// <summary>
        /// Cookie构建
        /// </summary>
        public CookieBuilder Cookie
        {
            get => _cookieBuilder;
            set => _cookieBuilder = value ?? throw new ArgumentNullException(nameof(value));
        }


        /// <summary>
        /// 
        /// </summary>
        private class BrowserCookieBuilder : CookieBuilder
        {
            public BrowserCookieBuilder()
            {
                Name = BrowserDefaults.CookieName;
                Path = BrowserDefaults.CookiePath;
                HttpOnly = true;
                IsEssential = false;
            }


        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class BrowserDefaults
    {
        /// <summary>
        /// 
        /// </summary>
        public static readonly string CookieName = ".browser.id";

        /// <summary>
        /// 
        /// </summary>
        public static readonly string CookiePath = "/";
    }





}
