﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Model
{
    /// <summary>
    /// 后台管理员
    /// </summary>
    [Serializable]
    public partial class Administrator
    {
        /// <summary>
        /// 用户id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 用户账号
        /// </summary>
        public virtual string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary> 
        public virtual string ChsName { get; set; }

        /// <summary>
        /// 手机号
        /// </summary> 
        public virtual string MobilePhone { get; set; }
         
        /// <summary>
        /// 超级管理员？
        /// </summary>
        public virtual bool IsAdmin { get; set; }

        /// <summary>
        /// 禁用？
        /// </summary>
        public virtual bool IsDisabled { get; set; }
    }
}
