﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Model
{
    /// <summary>
    /// 功能菜单
    /// </summary>
    [Serializable]
    public partial class CategoryInfo
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 是否是菜单
        /// </summary>
        public virtual bool IsMenu { get; set; }

        /// <summary>
        /// 唯一标识
        /// </summary>
        public virtual string SysResource { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string ResourceID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string FatherID { get; set; }

        /// <summary>
        /// 控制器
        /// </summary>
        public virtual string Controller { get; set; }

        /// <summary>
        /// 方法
        /// </summary>
        public virtual string Action { get; set; }

        /// <summary>
        /// 路由名称
        /// </summary>
        public virtual string RouteName { get; set; }

        /// <summary>
        /// 样式
        /// </summary>
        public virtual string CssClass { get; set; }

        /// <summary>
        /// 排序
        /// </summary>
        public virtual int Sort { get; set; }
    }
}
