﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Model
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public partial class Store
    {
        /// <summary>
        /// 编号id
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 暂停使用后台
        /// </summary>
        public virtual bool Stop { get; set; }
    }
}
