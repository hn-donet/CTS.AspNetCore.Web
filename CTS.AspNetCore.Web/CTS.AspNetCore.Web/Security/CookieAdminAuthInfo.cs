﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Security
{
    /// <summary>
    /// 登陆状态 cookie 保存详情
    /// services.AddAuthentication 配置使用
    /// </summary>
    public class CookieAdminAuthInfo
    {
        /// <summary>
        /// 后台登录状态的cookie名称
        /// </summary>
        public readonly static string AuthenticationScheme = "AdminAuth";

        /// <summary>
        /// 
        /// </summary>
        public readonly static string CookiePrefix = ".AspNetCore.";

        /// <summary>
        ///  发行人
        /// </summary>
        public readonly static string ClaimsIssuer = "AspNetCore";

    }
}
