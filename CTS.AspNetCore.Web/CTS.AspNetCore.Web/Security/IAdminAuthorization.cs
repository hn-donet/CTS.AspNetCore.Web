﻿using Microsoft.AspNetCore.Mvc.Filters;
using AspNetCore.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Security
{
    /// <summary>
    /// 
    /// </summary>
    public interface IAdminAuthorization
    {

        /// <summary>
        /// 登陆后可获取token
        /// </summary>
        string Token { get; }

        /// <summary>
        /// 保存登陆状态
        /// </summary>
        /// <param name="name">姓名</param>
        /// <param name="func">委托，在保存cookie前执行 返回tokenId</param>
        void SignIn(string name, Func<string> func);

        /// <summary>
        /// 退出登录
        /// </summary>
        void SignOut();

        /// <summary>
        /// 获取当前登录用户
        /// （缓存）
        /// 委托参数：tokenId
        /// </summary>
        /// <param name="func">委托，执行后返回：Administrator，参数为获取的tokenId</param>
        /// <returns></returns>
         Administrator GetCurrentUser(Func<string,Administrator> func);

        /// <summary>
        /// 获取我的权限数据
        /// </summary>
        /// <param name="func">委托，执行后返回：CategoryInfo 集合 </param>
        /// <returns></returns>
        List<CategoryInfo> GetMyCategories(Func<List<CategoryInfo>> func);
         
    }
}
