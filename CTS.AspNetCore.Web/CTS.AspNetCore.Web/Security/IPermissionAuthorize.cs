﻿using Microsoft.AspNetCore.Mvc.Filters;
using AspNetCore.Web.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace AspNetCore.Web.Security
{
    /// <summary>
    /// 用户权限验证
    /// </summary>
    public interface IPermissionAuthorize
    {
        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="context"></param>
        /// <param name="categoryInfos"></param>
        /// <returns></returns>
        bool AuthorizePermission(ActionExecutingContext context, List<CategoryInfo> categoryInfos);

        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="routeName"></param> 
        /// <param name="categoryInfos"></param>
        /// <returns></returns>
        bool AuthorizePermission(string routeName, List<CategoryInfo> categoryInfos);
    }
}
