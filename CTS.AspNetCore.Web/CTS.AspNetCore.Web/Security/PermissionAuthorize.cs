﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Filters;
using AspNetCore.Web.Model;

namespace AspNetCore.Web.Security
{
    /// <summary>
    /// 后台权限验证
    /// </summary>
    public class PermissionAuthorize : IPermissionAuthorize
    {
         
        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="context"></param>
        /// <param name="categoryInfos"></param>
        /// <returns></returns>
        public bool AuthorizePermission(ActionExecutingContext context, List<CategoryInfo> categoryInfos)
        {
            string action = context.ActionDescriptor.RouteValues["action"];
            string controller = context.ActionDescriptor.RouteValues["controller"];

            return categoryInfos != null && categoryInfos.Any(o => o.Controller != null && o.Action != null ||
              o.Controller.Equals(controller, StringComparison.InvariantCultureIgnoreCase)
              && o.Action.Equals(action, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// 权限验证
        /// </summary>
        /// <param name="routeName"></param>
        /// <param name="categoryInfos"></param>
        /// <returns></returns>
        public bool AuthorizePermission(string routeName, List<CategoryInfo> categoryInfos)
        {
            return categoryInfos != null && categoryInfos.Any(o => o.RouteName != null &&
             o.RouteName.Equals(routeName, StringComparison.InvariantCultureIgnoreCase));
        }
    }
}
