﻿namespace AspNetCore.Web.Security.Provider
{
    /// <summary>
    /// token存储Provider
    /// </summary>
    public interface ITokenProvider
    {
        /// <summary>
        /// 存储token cookie
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="token"></param>
        /// <param name="name">名称</param>
        /// <param name="claimsIssuer"></param>
        void Store(string scheme,string token, string name,string claimsIssuer);

        /// <summary>
        /// 获取token的值
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="headerKey"></param>
        /// <param name="claimsIssuer"></param>
        string GetToken(string scheme,string claimsIssuer, string headerKey);

        /// <summary>
        /// cookie 获取token值
        /// </summary>
        /// <param name="scheme"></param>
        /// <param name="claimsIssuer"></param>
        /// <returns></returns>
        string GetToken(string scheme, string claimsIssuer);

        /// <summary>
        /// 从 header获取token
        /// </summary>
        /// <param name="headerKey"></param>
        /// <returns></returns>
        string GetToken(string headerKey);
    }
}