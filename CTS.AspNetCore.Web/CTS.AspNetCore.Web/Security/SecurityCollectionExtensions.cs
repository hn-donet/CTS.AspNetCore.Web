﻿using Microsoft.Extensions.DependencyInjection;
using AspNetCore.Web.Security;
using AspNetCore.Web.Security.Provider;
using System;
using System.Collections.Generic;
using System.Text;
using AspNetCore.Web.Config;
using Microsoft.Extensions.Configuration;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// 
    /// </summary>
    public static class SecurityCollectionExtensions
    {
        /// <summary>
        /// 注册管理端安全验证服务 
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        /// <returns></returns>
        public static IServiceCollection AddAdminSecurity(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null)
            {
                throw new ArgumentNullException(nameof(services));
            }

            services.AddSingleton<IPermissionAuthorize, PermissionAuthorize>();
            services.AddSingleton<IAdminAuthorization, AdminAuthorization>();
            services.AddSingleton<ITokenProvider, TokenProvider>();

            var config = services.ConfigureStartupConfig<AspNetCoreConfig>(configuration.GetSection("AspNetCoreConfig"));
            services.AddAuthentication(o =>
            {
                o.DefaultAuthenticateScheme = CookieAdminAuthInfo.AuthenticationScheme;
                o.DefaultChallengeScheme = CookieAdminAuthInfo.AuthenticationScheme;
            }).AddCookie(CookieAdminAuthInfo.AuthenticationScheme, o =>
            {
                o.Cookie.Domain = config.Domain;
                o.Cookie.Name = CookieAdminAuthInfo.CookiePrefix + CookieAdminAuthInfo.AuthenticationScheme;
            });

            return services;
        }









    }
}
